<?php
/**
 * Sage includes
 *
 * The $sage_includes array determines the code library included in your theme.
 * Add or remove files to the array as needed. Supports child theme overrides.
 *
 * Please note that missing files will produce a fatal error.
 *
 * @link https://github.com/roots/sage/pull/1042
 */
$sage_includes = [
  'lib/assets.php',    // Scripts and stylesheets
  'lib/extras.php',    // Custom functions
  'lib/setup.php',     // Theme setup
  'lib/titles.php',    // Page titles
  'lib/wrapper.php',   // Theme wrapper class
  'lib/tgmpa.php' // TGMPA
];

foreach ($sage_includes as $file) {
  if (!$filepath = locate_template($file)) {
    trigger_error(sprintf(__('Error locating %s for inclusion', 'sage'), $file), E_USER_ERROR);
  }
  require_once $filepath;
}
unset($file, $filepath);

//MY FUNCTIONS START

function add_file_types_to_uploads($file_types){
	$new_filetypes = array();
	$new_filetypes['svg'] = 'image/svg+xml';
	$file_types = array_merge($file_types, $new_filetypes );
	return $file_types;
}
add_action('upload_mimes', 'add_file_types_to_uploads');

function create_tour_function() {
    $post = array(
        'post_status'  => 'draft',
        'post_title' => $_POST['title'],
        'post_type'  => 'tours'
    );
    $the_post_id = wp_insert_post($post);


    update_field('tour_id', $_POST['id'], $the_post_id);
    update_field('tour_author', $_POST['author'], $the_post_id);
    update_field('tour_image', $_POST['image'], $the_post_id);
}
add_action( 'wp_ajax_nopriv_create_tour_function',  'create_tour_function' );
add_action( 'wp_ajax_create_tour_function','create_tour_function' );

function status_tour_function() {
$query = new WP_Query( array( 'post_type' => 'tours',
    'posts_per_page' => -1,
    'meta_query' => array(
        'relation'		=> 'AND',
        array(
            'key'     => 'tour_id',
            'value'   => $_POST['id'],
            'compare' => '='
        )
    ),
    'orderby' => 'meta_value_num',
    'post_status' => array('draft','publish'),
    'paged' => $paged));
    if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
        if($_POST['status'] == 'Publish'){
            wp_update_post(array(
                'ID' =>  get_the_ID(),
                'post_status'   =>  'publish'
            ));
        }else{
            wp_update_post(array(
                'ID'    =>  get_the_ID(),
                'post_status'   =>  'draft'
            ));
        }
        endwhile; wp_reset_postdata();
    endif;
}
add_action( 'wp_ajax_nopriv_status_tour_function',  'status_tour_function' );
add_action( 'wp_ajax_status_tour_function','status_tour_function' );

function edit_tour_function() {
    $query = new WP_Query( array( 'post_type' => 'tours',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation'		=> 'AND',
            array(
                'key'     => 'tour_id',
                'value'   => $_POST['id'],
                'compare' => '='
            )
        ),
        'orderby' => 'meta_value_num',
        'post_status' => array('draft','publish'),
        'paged' => $paged));
    if ( $query->have_posts() ) :
        while ( $query->have_posts() ) : $query->the_post();
            wp_update_post(array(
                'ID' =>  get_the_ID(),
                'post_title' => $_POST['title'],
            ));
        endwhile; wp_reset_postdata();
    endif;
}
add_action( 'wp_ajax_nopriv_edit_tour_function',  'edit_tour_function' );
add_action( 'wp_ajax_edit_tour_function','edit_tour_function' );

function delete_tour_function() {
    wp_delete_post($_POST['id']);
}
add_action( 'wp_ajax_nopriv_delete_tour_function',  'delete_tour_function' );
add_action( 'wp_ajax_delete_tour_function','delete_tour_function' );

//MY FUNCTIONS END