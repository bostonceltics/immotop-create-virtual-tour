<div class="header">
    <a href="/" class="header_logo">
        <img src="<?=get_field('logo','option')['url'];?>" alt="<?=get_field('logo','option')['alt'];?>">
    </a>
</div>
<div class="error">
    <div class="error_wrap">
        <h1 class="error_title">404</h1>
        <p class="error_desc">Page not found</p>
        <a href="/" class="error_btn">Back to mainpage</a>
    </div>
</div>
