$(document).ready(function(){

    function createTour() {
        if (document.getElementById('evrifui')) {
            $('.create-mode #evrctb').on('click',function(){
                var id = $("#tour_id").text();
                var author = $("#tour_author").text();
                var title = $("#evrtn").val();
                $("#tour_title").text(title);
                var image = $("#evrifui li:eq(0) img:eq(1)").attr('src');
                $("#tour_image").text(image);

                var ajaxurl = 'https://'+window.location.host+'/wp-admin/admin-ajax.php';
                $.ajax({
                    url: ajaxurl + "?action=create_tour_function",
                    type:'POST',
                    data:{
                        id: id,
                        title: title,
                        author: author,
                        image: image
                    },
                    success: function(data){
                        console.log(data);
                    }
                });
            });

            $('.create-mode .evovr-close').on('click',function(){
                location.reload();
            });
            $('.evovr-close-editor').on('click',function(){
                location.reload();
            });
            $('#evrtesibch').attr('checked','false');
        } else {
            setTimeout(createTour, 15);
        }
    }
    createTour();

    function statusTour() {
        if (document.getElementById('evrtepbt')) {
            $('#evrtepbt').on('click',function(){
                var id = $("#tour_id").text();
                var status = $(this).text();
                var ajaxurl = 'https://'+window.location.host+'/wp-admin/admin-ajax.php';
                $.ajax({
                    url: ajaxurl + "?action=status_tour_function",
                    type:'POST',
                    data:{
                        id: id,
                        status: status
                    },
                    success: function(data){
                        console.log(data);
                    }
                });
                $('#evrtesibch').attr('checked','false');
            });
        } else {
            setTimeout(statusTour, 15);
        }
    }
    statusTour();

    $('.edit-tour').on('click',function(){
        $('#mode').text('edit-mode');
        $('#tour_id').text($(this).attr('postid'));
    });
    function getMode() {
        if (document.getElementById('evrifui')) {
            var mode = $("#mode").text();
            $("#evrimgc").addClass(mode);
        }else {
            setTimeout(getMode, 15);
        }
    }
    getMode();
    function editTour() {
        if (document.getElementById('evrifui')) {
            $('.edit-mode #evrctb').on('click',function(){
                var id = $("#tour_id").text();
                var title = $("#evrtn").val();
                $("#tour_title").text(title);

                var ajaxurl = 'https://'+window.location.host+'/wp-admin/admin-ajax.php';
                $.ajax({
                    url: ajaxurl + "?action=edit_tour_function",
                    type:'POST',
                    data:{
                        id: id,
                        title: title
                    },
                    success: function(data){
                        console.log(data);
                    }
                });
            });

            $('.edit-mode .evovr-close').on('click',function(){
                location.reload();
            });
        } else {
            setTimeout(editTour, 15);
        }
    }
    editTour();

    function updateMode() {
        if (document.getElementById('evrtrec')) {
            var mode = $("#mode").text();
            $("#evrtrec").addClass(mode);

            $('#evrtepbt').on('click',function(){
                $(this).toggleClass('active');
            });
            $('.edit-mode .evovr-close-editor').on('click',function(){
                location.reload();
            });
        } else {
            setTimeout(updateMode, 15);
        }
    }
    updateMode();

    $('.delete-tour').on('click',function(){
        var id = $(this).attr('postid');
        var ajaxurl = 'https://'+window.location.host+'/wp-admin/admin-ajax.php';
        $.ajax({
            url: ajaxurl + "?action=delete_tour_function",
            type:'POST',
            data:{
                id: id
            },
            success: function(data){
                console.log(data);
                location.reload();
            }
        });
    });

    $('html').attr('style','margin-top: 0!important');

	
	
	
	
});
