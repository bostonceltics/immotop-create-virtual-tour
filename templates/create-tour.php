<?php
$last_tour_id = 0;
$author = get_userdata(get_current_user_id())->user_email;
$query = new WP_Query( array( 'post_type' => 'tours',
    'posts_per_page' => -1,
    'meta_query' => array(
        'relation'		=> 'AND',
        array(
            'key'     => 'tour_author',
            'value'   => $author,
            'compare' => '='
        )
    ),
    'orderby' => 'meta_value_num',
    'post_status' => array('draft','publish'),
    'paged' => $paged));
if ( $query->have_posts() ) : ?>
    <?php while ( $query->have_posts() ) : $query->the_post(); ?>
        <?php $last_tour_id++;?>
    <?php endwhile; wp_reset_postdata(); ?>
<?php endif; ?>
<?php $last_tour_id++;?>
<a href="javascript:void(0);" postid="<?=get_current_user_id().$last_tour_id;?>" showGadget="false" class="main_btn create-tour evovr-tour-editor">Create new tour</a>

<div id="tour_id"><?=get_current_user_id().$last_tour_id;?></div>
<div id="tour_title"></div>
<div id="tour_desc"></div>
<div id="tour_image"></div>
<div id="tour_author"><?=get_userdata(get_current_user_id())->user_email;?></div>
<div id="mode"></div>

