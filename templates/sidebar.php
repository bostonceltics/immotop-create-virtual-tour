<div class="sidebar_avatar-wrap">
    <div class="sidebar_avatar" style="background-image: url(<?=get_avatar_url(get_current_user_id());?>)">
        <i class="fa fa-user"></i>
    </div>
</div>

<p class="sidebar_name"><?=get_userdata(get_current_user_id())->display_name;?></p>

<div class="sidebar_line"></div>

<div class="sidebar_box">
    <p class="sidebar_box-title">INFORMATION</p>
    <p class="sidebar_box-desc"><?=get_field('service_info','option');?></p>
</div>

