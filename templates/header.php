
<div class="header">
    <div class="header_row">
        <div class="header_col">
            <a href="/" class="header_logo">
                <img src="<?=get_field('logo','option')['url'];?>" alt="<?=get_field('logo','option')['alt'];?>">
            </a>
        </div>
        <div class="header_col">
            <ul class="header_menu">
                <li class="header_item">
                    <a href="/tours">Tours</a>
                </li>
                <li class="header_item">
                    <a href="/profil">My profil</a>
                </li>
                <li class="header_item">
                    <?php if (is_user_logged_in() ): ?>
                        <a href="<?=wp_logout_url('/');?>" class="header_btn">Log out</a>
                    <?php endif;?>
                </li>
            </ul>
        </div>
    </div>
</div>
