<?php get_template_part('templates/create-tour');?>

<table class="tours_table">
    <tbody>
    <?php
    $author = get_userdata(get_current_user_id())->user_email;
    $query = new WP_Query( array( 'post_type' => 'tours',
        'posts_per_page' => -1,
        'meta_query' => array(
            'relation'		=> 'AND',
            array(
                'key'     => 'tour_author',
                'value'   => $author,
                'compare' => '='
            )
        ),
        'orderby' => 'meta_value_num',
        'post_status' => array('draft','publish'),
        'paged' => $paged));
    if ( $query->have_posts() ) : ?>
        <?php while ( $query->have_posts() ) : $query->the_post(); ?>
            <tr class="<?php if( $post->post_status == 'draft' ){echo 'tours_draft';}else{echo 'tours_publish';}?>">
                <td><a href="<?php if( $post->post_status == 'draft' ){echo 'javascript:void(0);';}else{echo '/preview?id='.get_field('tour_id').'&post_id='.get_the_ID();}?>" class="tours_image" target="<?php if( $post->post_status == 'publish' ){echo '_blank';}?>" style="background-image: url(<?=get_field('tour_image');?>);"></a></td>
                <td><p class="tours_title"><?php the_title();?></p>
                    <p class="tours_mod">Created <?=get_the_date('d/m/Y');?></p>
                </td>
                <td>
                    <div class="td_wrap">
                        <span><?php if( $post->post_status == 'publish' ){echo 'publish';}?></span>
                        <a href="/video-call/?id=<?=get_field('tour_id');?>&post-id=<?=get_the_ID();?>" class="video-tour"><b>Video call</b><i class="fa fa-video"></i></a>
                        <a href="javascript:void(0);" postid="<?=get_field('tour_id');?>" showGadget="false" class="edit-tour evovr-tour-editor"><b>Edit tour</b><i class="fa fa-edit"></i></a>
                        <a href="javascript:void(0);" postid="<?=get_the_ID();?>" class="delete-tour"><b>Delete tour</b><i class="fa fa-trash-alt"></i></a>
                    </div>
                </td>
            </tr>
        <?php endwhile; wp_reset_postdata(); ?>
    <?php endif; ?>
    </tbody>
</table>

