<?php
/**
 * Template Name: Registration
 */
?>

<div class="header">
    <a href="/" class="header_logo">
        <img src="<?=get_field('logo','option')['url'];?>" alt="<?=get_field('logo','option')['alt'];?>">
    </a>
</div>
<div class="my-login">
    <div class="my-login_wrap">
        <h1 class="my-login_title">Registration</h1>
        <?= do_shortcode('[user_registration_form id="10" redirect_url="/tours"]'); ?>
        <?php if (is_user_logged_in() ): ?>
            <script>window.location.replace("/tours");</script>
        <?php endif;?>
        <a href="/">Back to login page</a>
    </div>
</div>