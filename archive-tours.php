<?php get_template_part('templates/header');?>
<div class="main">
    <div class="main_row">
        <div class="main_col">
            <?php get_template_part('templates/sidebar');?>
        </div>
        <div class="main_col">
            <h2 class="main_title">Tours</h2>
            <?php get_template_part('templates/tours');?>
        </div>
    </div>
</div>
