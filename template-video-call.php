<?php
/**
 * Template Name: Video call
 */
?>

<div id="meet"></div>
<script src='https://meet.jit.si/external_api.js'></script>
<script>
    const domain = 'meet.jit.si';
    const options = {
        roomName: 'video-immotop-lu-preview-id<?=$_GET['id'];?>post-id<?=$_GET['post-id'];?>',
        parentNode: document.querySelector('#meet'),
        configOverwrite: {},
        interfaceConfigOverwrite: {
            DEFAULT_WELCOME_PAGE_LOGO_URL: 'https://video.immotop.lu/wp-content/themes/live_wp/assets/build/images/logo.webp',
            DEFAULT_REMOTE_DISPLAY_NAME: 'Immotop agent'
        }
    };
    const api = new JitsiMeetExternalAPI(domain, options);
</script>
