<?php
/**
 * Template Name: Preview
 */
?>
<?php
$rest = substr(get_field('tour_image',$_GET['post_id']), 0, -6);
?>
<div class="preview_bg" style="background-image: url(<?=$rest;?>.jpg);"></div>
<div class="evovr-tour-loader preview_play" postid="<?=$_GET['id'];?>" showIFrame="false" showGadget="false"><i class="fa fa-play"></i></div>