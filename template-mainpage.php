<?php
/**
 * Template Name: login
 */
?>

<div class="header">
    <a href="/" class="header_logo">
        <img src="<?=get_field('logo','option')['url'];?>" alt="<?=get_field('logo','option')['alt'];?>">
    </a>
</div>
<div class="my-login">
    <div class="my-login_wrap">
        <h1 class="my-login_title">Please sign in</h1>
        <?= do_shortcode('[user_registration_login redirect_url="/tours"]'); ?>
        <?php if (is_user_logged_in() ): ?>
            <script>window.location.replace("/tours");</script>
        <?php endif;?>
    </div>
</div>

